Проект сгенеророван с помощью [Create React App](https://github.com/facebookincubator/create-react-app).

Для запуска необходимо настоить API на REST-сервисы (файл /src/api/config.js) либо запустить mock-сервер, например json-server

```
json-server --watch src/mocks/db.json --port 4000
``` 

Запуск проекта в режиме разработки - ```npm start```

Запуск тестов - ```npm test```

Сборка - ```npm run build```