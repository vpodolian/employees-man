import {Component} from 'react';
import PropTypes from 'prop-types';

class CustomeEditableRow extends Component {
    static propTypes = {
        edited: PropTypes.bool,
        cells: PropTypes.arrayOf(PropTypes.shape({
            asKey: PropTypes.bool, // content value will be used as key to pass in onRowClick function
            content: PropTypes.element
        }))
    }

    constructor (props) {
        super(props);
        state = {}
    }

    render () {
        const {cells} = this.props;
        
        return (
            <tr
                className="edited"
            >
                {
                    cells && cells.map(cell => (
                        <td
                            key={shortid.generate()}
                            onKeyPress={onEdit}
                        >
                            <cell.content onKeyPress={() => console.log('INPUT PRESSED!')}/>
                        </td>
                    ))
                }
            </tr>
        );
    }
}