import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';

import Sidebar from './Sidebar';
import {EmployeesTableContainer} from '../containers/EmployeesTableContainer';
import {DepartmentsTableContainer} from '../containers/DepartmentsTableContainer';

const gridStyles = {marginTop: '4rem'};

export const Layout = () => {
    return (
        <Grid style={gridStyles}>
            <Row>
                <Col sm={2}>
                    <Sidebar />
                </Col>
                <Col sm={10}>
                    <Switch>
                        <Route path="/employees" component={EmployeesTableContainer} />
                        <Route path="/departments" component={DepartmentsTableContainer} />
                        <Redirect to="/employees" />
                    </Switch>
                </Col>
            </Row>
        </Grid>
    );
} 