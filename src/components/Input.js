import React from 'react';
import PropTypes from 'prop-types';

import '../styles/input.css';

export class Input extends React.Component {
    static PropTypes = {
        value: PropTypes.string,
        onEdit: PropTypes.func
    }

    constructor (props) {
        super(props);

        this.state = {
            value: props.value || '',
            isChanged: false,
            isFocused: false
        }
    }

    componentDidUpdate () {
        this.state.isFocused && this.input.focus();
    }

    completeEdit = () => {
        this.state.isChanged && this.props.onEdit && this.props.onEdit(this.state.value);
    } 

    handleOnBlur = () => {
        this.completeEdit();
        this.setState({
            isFocused: false
        })
    }

    handleOnChange = (e) => {
        this.setState({
            value: e.target.value,
            isChanged: true
        })
    }

    handleKeyPress = (e) => {
        if (e.charCode === 13) {
            this.completeEdit();
        }
    }

    render () {
        return (
            <input
                ref={(input) => this.input = input}
                className="input"
                value={this.state.value}
                onChange={this.handleOnChange}
                onBlur={this.handleOnBlur}
                onKeyPress={this.handleKeyPress}
                onFocus={() => this.setState({ isFocused: true })}
            />
        );
    }
}