import React from 'react';
import PropTypes from 'prop-types';
import Table from 'react-bootstrap/lib/Table';
import shortid from 'shortid';

import {CustomTableRow} from './CustomTableRow';

export const CustomTable = ({headers, rows, onRowClick, onKeyPress}) => (
  <Table responsive>
    <thead>
      <tr>
        {headers && headers.map(header => <th key={shortid.generate()}>{header.text}</th>)}
      </tr>
    </thead>
    <tbody>
      {
        rows && rows.map(row => (
          <CustomTableRow
            key={shortid.generate()}
            edited={row.edited}
            cells={row.cells}
            onClick={onRowClick}
            onKeyPress={onKeyPress}
          />
        ))
      }
    </tbody>
  </Table>
);

CustomTable.PropTypes = {
  header: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string
  })).isRequired,
  rows: PropTypes.arrayOf(PropTypes.shape({
    edited: PropTypes.bool,
    cells: PropTypes.arrayOf(PropTypes.shape({
      asKey: PropTypes.bool, // content value will be used as key to pass in onRowClick function
      content: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
      ])
    }))
  })).isRequired,
  onRowClick: PropTypes.func,
  onKeyPress: PropTypes.func
}