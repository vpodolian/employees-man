import React from 'react';

import {CustomTable} from './CustomTable';

export const TableContainer = () => (
    <div>
        <CustomTable
            headers={[
                {text: '#'},
                {text: 'Heading'},
                {text: 'Heading'},
                {text: 'Heading'},
                {text: 'Heading'},
                {text: 'Heading'},
                {text: 'Heading'}
            ]}
            rows={[
                {
                    edited: true,
                    cells: [
                        { content: 1 },
                        { content: 'text' },
                        { content: 'text' },
                        { content: 'text' },
                        { content: 'text' },
                        { content: <input /> },
                        { content: <input /> }
                    ]
                }
            ]}
            onRowClick={() => console.log('Clicked!!!')}
            onRowEdit={() => console.log('Pressed')}
        />
    </div>
)
