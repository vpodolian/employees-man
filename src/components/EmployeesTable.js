import React from 'react';
import onClickOutside from 'react-onclickoutside';

import {CustomTable} from './CustomTable';
import {Input} from './Input';

const EmployeesTable = ({employees, editedEmployee, onRowClick, onFieldEdit, onDelete}) => {

    const rows = employees && employees.map(employee => {
        const edited = editedEmployee ? employee.id === editedEmployee.id : false;
        let cells = [];

        if (!edited) {
            cells = [
                { content: employee.id, asKey: true },
                { content: employee.firstName },
                { content: employee.lastName },
                { content: employee.departmentId },
            ]
        } else {
            cells = [
                { content: employee.id, asKey: true },
                { content: <Input value={employee.firstName} onEdit={onFieldEdit(employee.id, 'firstName')} />},
                { content: <Input value={employee.lastName} onEdit={onFieldEdit(employee.id, 'lastName')} />},
                { content: <Input value={employee.departmentId} onEdit={onFieldEdit(employee.id, 'departmentId')} /> },
                { content: <button onClick={() => onDelete(employee.id)}>Delete</button> }
            ]
        }

        return {
            edited,
            cells
        }
    });

    const handleKeyPress = (e) => {
        if (e.charCode === 13) {
            onRowClick();
        }
    }

    const headers = [
        { text: '#' },
        { text: 'First Name' },
        { text: 'Last Name' },
        { text: 'Department' }
    ]

    if (editedEmployee) {
        headers.push({ text: '' });
    }

    return (
        <div>
            <CustomTable
                headers={headers}
                rows={rows}
                onRowClick={onRowClick}
                onKeyPress={handleKeyPress}
            />
        </div>
    )
}

export default onClickOutside(EmployeesTable);