import React from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom'
import Button from 'react-bootstrap/lib/Button';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';

const Sidebar = (props) => (
    <ButtonGroup vertical block>
        <Button onClick={() => props.history.push('/employees')}>Employees</Button>
        <Button onClick={() => props.history.push('/departments')}>Departments</Button>
    </ButtonGroup>
)

Sidebar.PropTypes = {
    history: PropTypes.shape({
        push: PropTypes.array
    }).isRequired
}

export default withRouter(Sidebar);