import React from 'react';
import shortid from 'shortid';

export const CustomTableRow = ({edited, cells, onClick, onEdit, onKeyPress}) => {

    const keyCell = cells && cells.find(cell => cell.asKey === true)

    const handleRowClick = () => {
        if (!keyCell) {
            console.warn("Key cell is not defined. Please specify asKey field in the one of the cells");
            return;
        }

        if (!edited) {
            onClick(keyCell.content);
        }
    }

    return (
        <tr
            className={edited ? 'edited' : ''}
            onClick={handleRowClick}
        >
            {
                cells && cells.map(cell => (
                    <td
                        key={shortid.generate()}
                        onKeyPress={onKeyPress}
                    >
                        {cell.content}
                    </td>
                ))
            }
        </tr>
    );
}