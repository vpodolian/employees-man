import React from 'react';
import onClickOutside from 'react-onclickoutside';

import {CustomTable} from './CustomTable';
import {Input} from './Input';

const DepartmentsTable = ({departments, editedDepartment, onRowClick, onFieldEdit, onDelete}) => {

    const rows = departments && departments.map(department => {
        const edited = editedDepartment ? department.id === editedDepartment.id : false;
        let cells = [];

        if (!edited) {
            cells = [
                { content: department.id, asKey: true },
                { content: department.name },
            ]
        } else {
            cells = [
                { content: department.id, asKey: true },
                { content: <Input value={department.name} onEdit={onFieldEdit(department.id, 'name')} /> },
                { content: <button onClick={() => onDelete(department.id)}>Delete</button> }
            ]
        }

        return {
            edited,
            cells
        }
    });

    const handleKeyPress = (e) => {
        if (e.charCode === 13) {
            onRowClick();
        }
    }

    const headers = [
        { text: '#' },
        { text: 'Name' },
    ]

    if (editedDepartment) {
        headers.push({text: ''});
    }

    return (
        <div>
            <CustomTable
                headers={headers}
                rows={rows}
                onRowClick={onRowClick}
                onKeyPress={handleKeyPress}
            />
        </div>
    )
}

export default onClickOutside(DepartmentsTable);
