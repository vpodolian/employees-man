import React from 'react';
import {connect} from "react-redux";

import EmployeeActions from '../actions/EmployeesActions';
import EmployeesTable from '../components/EmployeesTable';

class TableContainer extends React.Component {
    
    componentWillMount () {
        const {getEmployees} = this.props;
        getEmployees && getEmployees();
    }

    render () {
        return (
            <div>
                <EmployeesTable
                    {...this.props}
                />
                <button
                    className="pull-right"
                    onClick={this.props.onAdd}
                >
                    Add
                </button>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    employees: state.employees.list,
    editedEmployee: state.employees.editedEmployee
})

const mapDispatchToProps = (dispatch) => {
    const actions = new EmployeeActions();

    return {
        getEmployees: () => dispatch(actions.getEmloyees()),
        onRowClick: (employeeId) => dispatch(actions.setEditedEmployee(employeeId)),
        onFieldEdit: (employeeId, field) => (value) => dispatch(actions.updateEmployee({
            id: employeeId,
            [field]: value
        })),
        /*
         * Here, we reset any edited row when outside clicked,
         * handleClickOutside name is defined by the react-onclickoutside lib's name convention.
         */
        handleClickOutside: () => dispatch(actions.setEditedEmployee()),
        onAdd: () => dispatch(actions.addEmployee()),
        onDelete: (employeeId) => dispatch(actions.deleteEmployee(employeeId))
    }
}

export const EmployeesTableContainer = connect(mapStateToProps, mapDispatchToProps)(TableContainer)