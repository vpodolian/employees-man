import React from 'react';
import {connect} from "react-redux";

import {DepartmentsActions} from '../actions';
import DepartmentsTable from '../components/DepartmentsTable';

class TableContainer extends React.Component {
    
    componentWillMount () {
        const {getDepartments} = this.props;
        getDepartments && getDepartments();
    }

    render () {
        return (
            <div>
                <DepartmentsTable
                    {...this.props}
                />
                <button
                    className="pull-right"
                    onClick={this.props.onAdd}
                >
                    Add
                </button>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    departments: state.departments.list,
    editedDepartment: state.departments.editedDepartment
})

const mapDispatchToProps = (dispatch) => {
    const actions = new DepartmentsActions();
    
    return {
        getDepartments: () => dispatch(actions.getDepartments()),
        onRowClick: (departmentId) => dispatch(actions.setEditedDepartment(departmentId)),
        onFieldEdit: (departmentId, field) => (value) => dispatch(actions.updateDepartment({
            id: departmentId,
            [field]: value
        })),
        /*
         * Here, we reset any edited row when outside clicked,
         * handleClickOutside name is defined by the react-onclickoutside lib's name convention.
         */
        handleClickOutside: () => dispatch(actions.setEditedDepartment()),
        onAdd: () => dispatch(actions.addDepartment()),
        onDelete: (departmentId) => dispatch(actions.deleteDepartment(departmentId))
    }
}

export const DepartmentsTableContainer = connect(mapStateToProps, mapDispatchToProps)(TableContainer)