import {restUrl} from './config';

const apiUrl = `${restUrl}/departments`;

export default class DepartmentsApi {

    async getDepartments () {
        const response = await fetch(apiUrl);
        return response.json();
    }

    async updateDepartment (department) {
        const response = await fetch(`${apiUrl}/${department.id}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(department)
        });
        return response.json();
    }

    async addDepartment () {
        const response = await fetch(`${apiUrl}`, {
            method: 'POST'
        });
        return response.json();
    }

    async deleteDepartment (id) {
        const response = await fetch(`${apiUrl}/${id}`, {
            method: 'DELETE'
        });
        return response.json(); 
    }
}