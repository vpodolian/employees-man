import {restUrl} from './config';

const apiUrl = `${restUrl}/employees`

export class EmployeesApi {

    async getEmployees () {
        const response = await fetch(`${apiUrl}`);
        return response.json();
    }

    async updateEmployee (employee) {
        const response = await fetch(`${apiUrl}/${employee.id}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(employee)
        });
        return response.json();
    }

    async addEmployee () {
        const response = await fetch(`${apiUrl}`, {
            method: 'POST'
        });
        return response.json();
    }

    async deleteEmployee (id) {
        const response = await fetch(`${apiUrl}/${id}`, {
            method: 'DELETE'
        });
        return response.json(); 
    }
}