import {GET_DEPARTMENTS, SET_EDITED_DEPARTMENT, UPDATE_DEPARTMENT, ADD_DEPARTMENT, DELETE_DEPARTMENT, SUCCEEDED} from '../actions/types';

const initialState = {
    list: [],
    editedDepartment: null
}

export const departmentsReducer = (state = initialState, action) => {
    if (action.type === `${GET_DEPARTMENTS}${SUCCEEDED}`) {
        return {
            ...state,  
            list: action.payload
        }
    }

    if (action.type === SET_EDITED_DEPARTMENT) {
        return {
            ...state,
            editedDepartment: state.list.find(department => department.id === action.payload)
        }
    }

    if (action.type === `${UPDATE_DEPARTMENT}${SUCCEEDED}`) {
        return {
            ...state,
            list: state.list.map(department => {
                if (department.id === action.payload.id) {
                    return {
                        ...department,
                        ...action.payload
                    }
                }

                return department;
            })
        }
    }

    if (action.type === `${ADD_DEPARTMENT}${SUCCEEDED}`) {
        return {
            ...state,
            list: [
                ...state.list,
                action.payload
            ],
            editedDepartment: action.payload
        }
    }

    if (action.type === `${DELETE_DEPARTMENT}${SUCCEEDED}`) {
        return {
            ...state,
            list: state.list.filter(department => department.id !== action.payload)
        }
    }

    return state;
}