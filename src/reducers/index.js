import {combineReducers} from 'redux';

import {employeesReducer} from './employeesReducer';
import {departmentsReducer} from './departmentsReducer';

export default combineReducers({
    employees: employeesReducer,
    departments: departmentsReducer
})