import {GET_EMPLOYEES, SET_EDITED_EMPLOYEE, UPDATE_EMPLOYEE, ADD_EMPLOYEE, DELETE_EMPLOYEE, SUCCEEDED} from '../actions/types';

const initialState = {
    list: [],
    editedEmployee: null
}

export const employeesReducer = (state = initialState, action) => {
    if (action.type === `${GET_EMPLOYEES}${SUCCEEDED}`) {
        return {
            ...state,  
            list: action.payload
        }
    }

    if (action.type === SET_EDITED_EMPLOYEE) {
        return {
            ...state,
            editedEmployee: state.list.find(employee => employee.id === action.payload)
        }
    }

    if (action.type === `${UPDATE_EMPLOYEE}${SUCCEEDED}`) {
        return {
            ...state,
            list: state.list.map(employee => {
                if (employee.id === action.payload.id) {
                    return {
                        ...employee,
                        ...action.payload
                    }
                }

                return employee;
            })
        }
    }

    if (action.type === `${ADD_EMPLOYEE}${SUCCEEDED}`) {
        return {
            ...state,
            list: [
                ...state.list,
                action.payload
            ],
            editedEmployee: action.payload
        }
    }

    if (action.type === `${DELETE_EMPLOYEE}${SUCCEEDED}`) {
        return {
            ...state,
            list: state.list.filter(employee => employee.id !== action.payload)
        }
    }

    return state;
}