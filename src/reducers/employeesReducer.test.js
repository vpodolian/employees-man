import {employeesReducer} from './employeesReducer';
import {GET_EMPLOYEES, SET_EDITED_EMPLOYEE, UPDATE_EMPLOYEE, SUCCEEDED} from '../actions/types';

const initialSate = {
    list: [],
    editedEmployee: null
}

it('returns employees list on GET_EMPLOYEES_SUCCEEDED action', () => {
    const payload = [
        { 
            firstName: 'Test',
            lastName: 'Testov'
        },
        { 
            firstName: 'Rest',
            lastName: 'Restov'
        }
    ];

    const nextState = employeesReducer(initialSate, { type: `${GET_EMPLOYEES}${SUCCEEDED}`, payload });
    expect(nextState).toEqual({
        ...initialSate,
        list: payload
    });
});

it('returns an employee to edit on SET_EDITED_EMPLOYEE action', () => {
    const employees = [
        { 
            id: 1,
            firstName: 'Test',
            lastName: 'Testov'
        },
        { 
            id: 2,
            firstName: 'Rest',
            lastName: 'Restov'
        }
    ];

    const state = {
        ...initialSate,
        list: employees
    }

    const nextState = employeesReducer(state, { type: SET_EDITED_EMPLOYEE, payload: 2 });
    expect(nextState).toEqual({
        ...state,
        editedEmployee: employees[1]
    });
})

it('update an employee on UPDATE_EMPLOYEE_SUCCEEDED action', () => {
    const employees = [
        { 
            id: 1,
            firstName: 'Test',
            lastName: 'Testov'
        },
        { 
            id: 2,
            firstName: 'Rest',
            lastName: 'Restov'
        }
    ];

    const state = {
        ...initialSate,
        list: employees
    }

    const employeeToUpdate = {
        id: 2, firstName: 'Rest Test'
    };

    const nextState = employeesReducer(state, { type: `${UPDATE_EMPLOYEE}${SUCCEEDED}`, payload: employeeToUpdate });
    expect(nextState).toEqual({
        ...state,
        list: [
            { 
                id: 1,
                firstName: 'Test',
                lastName: 'Testov'
            },
            { 
                id: 2,
                firstName: employeeToUpdate.firstName,
                lastName: 'Restov'
            }
        ]
    });
})