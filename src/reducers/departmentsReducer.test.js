import {departmentsReducer} from './departmentsReducer';
import {GET_DEPARTMENTS, SET_EDITED_DEPARTMENT, UPDATE_DEPARTMENT, SUCCEEDED} from '../actions/types';

const initialSate = {
    list: [],
    editedDepartment: null
}

it('returns departments list on GET_DEPARTMENTS_SUCCEEDED action', () => {
    const initialSate = {
        list: []
    }
    const payload = [
        { name: 'test'}
    ];

    const nextState = departmentsReducer(initialSate, { type: `${GET_DEPARTMENTS}${SUCCEEDED}`, payload });
    expect(nextState).toEqual({ list: payload });
})

it('returns department to edit on SET_EDITED_DEPARTMENT action', () => {
    const departments = [
        { 
            id: 1,
            name: 'Test',
        },
        { 
            id: 2,
            name: 'Rest',
        }
    ];

    const state = {
        ...initialSate,
        list: departments
    }

    const nextState = departmentsReducer(state, { type: SET_EDITED_DEPARTMENT, payload: 1 });
    expect(nextState).toEqual({
        ...state,
        editedDepartment: departments[0]
    });
})

it('update an employee on UPDATE_DEPARTMENT_SUCCEEDED action', () => {
    const departments = [
        { 
            id: 1,
            name: 'Test',
        },
        { 
            id: 2,
            name: 'Rest',
        }
    ];

    const state = {
        ...initialSate,
        list: departments
    }

    const departmentToUpdate = {
        id: 1, name: 'Test Done'
    };

    const nextState = departmentsReducer(state, { type: `${UPDATE_DEPARTMENT}${SUCCEEDED}`, payload: departmentToUpdate });
    expect(nextState).toEqual({
        ...state,
        list: [
            { 
                id: 1,
                name: 'Test Done'
            },
            { 
                id: 2,
                name: 'Rest'
            }
        ]
    });
})
