import {createActionThunk} from 'redux-thunk-actions';

import {EmployeesApi} from '../api/EmployeesApi';
import {GET_EMPLOYEES, UPDATE_EMPLOYEE, SET_EDITED_EMPLOYEE, ADD_EMPLOYEE, DELETE_EMPLOYEE} from './types';

export default class EmployeesActions {
    constructor () {
        this.api = new EmployeesApi();
    }

    getEmloyees = createActionThunk(GET_EMPLOYEES, () => this.api.getEmployees())

    updateEmployee = createActionThunk(UPDATE_EMPLOYEE, (employee, {getState}) => {
        const state = getState();
        const editedEmployee = state.employees.editedEmployee;
        
        return this.api.updateEmployee({
            ...editedEmployee,
            ...employee
        });
    })

    setEditedEmployee = (id) => ({ type: SET_EDITED_EMPLOYEE, payload: id })

    addEmployee = createActionThunk(ADD_EMPLOYEE, () => this.api.addEmployee())

    deleteEmployee = createActionThunk(DELETE_EMPLOYEE, (id) => {
        return this.api.deleteEmployee(id).then(() => id)
    })
}