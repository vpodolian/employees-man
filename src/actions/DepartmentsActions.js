import {createActionThunk} from 'redux-thunk-actions';

import DepartmentsApi from '../api/DepartmentsApi';
import {GET_DEPARTMENTS, UPDATE_DEPARTMENT, SET_EDITED_DEPARTMENT, ADD_DEPARTMENT, DELETE_DEPARTMENT} from './types';

export default class DepartmentsActions {
    constructor () {
        this.api = new DepartmentsApi();
    }

    getDepartments = createActionThunk(GET_DEPARTMENTS, () => this.api.getDepartments());

    updateDepartment = createActionThunk(UPDATE_DEPARTMENT, (department, {getState}) => {
        const state = getState();
        const editedDepartment = state.departments.editedDepartment;
        
        return this.api.updateDepartment({
            ...editedDepartment,
            ...department
        });
    });

    setEditedDepartment = (id) => ({ type: SET_EDITED_DEPARTMENT, payload: id })

    addDepartment = createActionThunk(ADD_DEPARTMENT, () => this.api.addDepartment())
    
    deleteDepartment = createActionThunk(DELETE_DEPARTMENT, (id) => {
        return this.api.deleteDepartment(id).then(() => id)
    })
} 